import { createApp } from 'vue'
import App from './App.vue'
import store from "./store";
import { createPinia } from 'pinia'

import './assets/main.css'
import './assets/index.css'

createApp(App)
//Add the line below to the file
.use(store)
.use(createPinia())
.mount('#app')
