import { createStore } from 'vuex'
import axios from 'axios'
export default createStore({
    state: {
        news: []
    },
    getters: {
      getNews: (state) => state.news
    },
    actions: {
      async fetchData({ commit }, filterInfo) {
          try {
            const data = await axios.get('https://newsapi.org/v2/top-headlines?country=' + filterInfo.country + '&category=' + filterInfo.category + '&apiKey=6e1bc4462411427d8cb051d429f52fbb')
              commit('SET_NEWS', data.data)
            }
            catch (error) {
                alert(error)
                console.log(error)
            }
        }
    },
    mutations: {
      SET_NEWS(state, news) {
          state.news = news
      }
    }
})